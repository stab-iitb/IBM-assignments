var express = require('express');
var router = express.Router();


/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index');
});

router.get('/lender_signup', function(req, res, next) {
  res.render('lender_signup', { title: 'Express' });
});

router.get('/borrower_signup', function(req, res, next) {
  res.render('borrower_signup', { title: 'Express' });
});

router.get('/lender_signin', function(req, res, next) {
  res.render('lender_signin', { title: 'Express' });
});

router.get('/borrower_signin', function(req, res, next) {
  res.render('borrower_signin', { title: 'Express' });
});

router.post('/borrower_signin_post', function(req, res, next) {
    var db = req.db;
    var collection1 = db.get('borrowers');
    var email=req.body.email_id;
    var pass=req.body.password;

    collection1.find({"email_id":email},{},function(e,borrower){
        if(typeof borrower==="undefined" || borrower.length == 0){
            res.render('borrower_signin',{
              error:"Invalid Email id"
            });
            return;
        }
        borrower=borrower[0];
        if(borrower.password==req.body.password){
            res.render('borrower_home', {
                borrower : borrower            
            });
        }
        else{
            res.render('borrower_signin',{
              error:"Wrond Email id & Password combination"
            });
        }  
    });
});    

router.post('/lender_signin_post', function(req, res, next) {
  //var firstn = req.body.firstname;
    var db = req.db;
    var collection1 = db.get('lenders');
    var email=req.body.email_id;
    var pass=req.body.password;

    collection1.find({email_id:email},{},function(e,lender){
        if(typeof lender==="undefined" || lender.length == 0){
            res.render('lender_signin',{
              error:"Invalid Email id"
            });
            return;
        }

        lender=lender[0];
        if(lender.password==req.body.password){
            res.render('lender_home', {
                lender : lender            
            });
        }
        else{
            res.render('lender_signin',{
              error:"Wrond Email id & Password combination"
            });
        }  
    });
  });


router.post('/borrower_signup_post', function(req, res, next) {
	//var firstn = req.body.firstname;
	//console.log(firstn);
	//res.send(firstn);
    var db = req.db;
    var collection = db.get('borrowers');

	collection.insert(req.body, function (err, doc) {
        if (err) {
            // If it failed, return error
            res.send(err);
        }
        else {
            // If it worked, set the header so the address bar doesn't still say /adduser
            //res.location("index");
            // And forward to success page
            //res.redirect("index");
            console.log("success");
              res.send("Hi "+req.body.firstname+". You are added in our Borrower Database");
        }

  });

  //res.render('index', { title: 'Express' });
    });    

router.post('/lender_signup_post', function(req, res, next) {
  //var firstn = req.body.firstname;
  //console.log(firstn);
  //res.send(firstn);
    var db = req.db;
    var collection = db.get('lenders');

  collection.insert(req.body, function (err, doc) {
        if (err) {
            // If it failed, return error
            res.send(err);
        }
        else {
            // If it worked, set the header so the address bar doesn't still say /adduser
            //res.location("index");
            // And forward to success page
            //res.redirect("index");
            console.log("success");
              res.send("Hi "+req.body.firstname+". You are added in our Lender Database");

        }
  });
  //res.render('index', { title: 'Express' });
});    

module.exports = router;
